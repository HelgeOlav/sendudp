# sendudp

A small tool to send/test UDP packets on the network. Specify

* File to send (max 64k - limit of IP packet size)
* Where to send it
* Number of times to send the packet
* Delay between each packet

To compile for Windows:
```
sudo GOOS=windows GOARCH=amd64
```
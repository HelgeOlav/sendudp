package main

import (
	"flag"
	"os"
)

var (
	addr    = flag.String("address", "localhost:8888", "where to send packet")
	input   = flag.String("file", "", "file to send")
	repeats = flag.Int("repeats", 1, "number of repetitions")
	delay   = flag.Int("delay", 0, "delay in ms between each packet, 0=no delay")
	threads = flag.Int("threads", 1, "sending threads")
)

func main() {
	flag.Parse()
	if input == nil || *input == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// check sending threads
	if *threads < 1 {
		*threads = 1
	}

	// check that repeats is grater than 0
	if *repeats < 1 {
		*repeats = 1
	}

	// check that delay is positive
	if *delay < 0 {
		*delay = 0
	}
	send()
}

package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

func send() {
	dur := time.Duration(int64(*delay) * int64(time.Millisecond))
	var counter, sentBytes uint64
	threadCount := 0
	var wg sync.WaitGroup

	// read input
	bytes, err := ioutil.ReadFile(*input)
	if err != nil {
		fmt.Println(err)
		return
	}

	// sending function
	sender := func(repeats int, id int) {
		defer wg.Done()
		// return if no messages to send
		if repeats < 1 {
			return
		}
		// create connection
		conn, err := net.Dial("udp", *addr)
		if err != nil {
			fmt.Println(id, err)
			return
		}
		defer conn.Close()
		for {
			n, err := conn.Write(bytes)
			if err != nil {
				fmt.Println(id, err)
				return
			}
			// count up and exit if done
			atomic.AddUint64(&counter, 1)
			atomic.AddUint64(&sentBytes, uint64(n))
			repeats--
			if repeats == 0 {
				return
			}
			// sleep
			if dur > 0 {
				time.Sleep(dur)
			}
		}
	}

	// start sending threads
	if *threads == 1 || *repeats == 1 {
		wg.Add(1)
		threadCount++
		go sender(*repeats, threadCount)
	} else {
		msgPerThread := *repeats / (*threads - 1)
		msgRemainder := *repeats % (*threads - 1)
		fmt.Println(msgPerThread, msgRemainder)
		// do the remainder
		if msgRemainder > 0 {
			threadCount++
			wg.Add(1)
			go sender(msgRemainder, threadCount)
		}
		// do the other threads
		for threadCount < *threads {
			threadCount++
			wg.Add(1)
			go sender(msgPerThread, threadCount)
		}
	}

	// Wait for senders to be done
	wg.Wait()

	// print result
	fmt.Printf("ok: Sent %v bytes to %v (%v times using %v threads)\n", sentBytes, *addr, counter, threadCount)

}
